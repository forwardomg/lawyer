@servers(['web' => 'deploy@zhogov.me'])

@task('deploy')
    cd /var/www/lawyer.zhogov.me
    git pull --ff-only origin master
@endtask