<?php
/*
Template Name: Template main
*/

get_header(); ?>

<div class="numbers">
  <div class="columns">
    <div class="column is-4">
      <div class="numbers-single"><span>8</span><?php pll_e('лет в интеллектуальной собственности'); ?></div>
    </div>
    <div class="column is-4">
      <div class="numbers-single"><span>420</span><?php pll_e('зарегистрированных объектов интеллектуальной собственности'); ?></div>
    </div>
    <div class="column is-4">
      <div class="numbers-single"><span>90</span><?php pll_e('довольных клиентов'); ?></div>
    </div>
  </div>
</div>

<div class="content">

  <?php
    while ( have_posts() ) : the_post();
      get_template_part( 'content', 'page' );
    endwhile;
  ?>

</div>

<div class="slider">
  <h2 class="slider-title"><?php pll_e('Мои<br>партнеры:'); ?></h2>
  <div class="slider-control">
    <a href="#" class="slider-right"><i class="icon icon-arrow-right"></i></a>
    <a href="#" class="slider-left"><i class="icon icon-arrow-left"></i></a>
  </div>
  <ul class="bxslider">
    <li><img src="/wp-content/uploads/2016/09/msu.png" alt=""></li>
    <li><img src="/wp-content/uploads/2016/09/PSU.jpg" alt=""></li>
    <li><img src="/wp-content/uploads/2016/09/PSTU.jpg" alt=""></li>
    <li><img src="/wp-content/uploads/2016/09/HSE.jpg" alt=""></li>
    <li><img src="/wp-content/uploads/2016/09/RT.png" alt=""></li>
    <li><img src="/wp-content/uploads/2016/09/megafon.png" alt=""></li>
    <li><img src="/wp-content/uploads/2016/09/pnppk.jpg" alt=""></li>
    <li><img src="/wp-content/uploads/2016/09/Lighting-tech-1.jpg" alt=""></li>
    <li><img src="/wp-content/uploads/2016/09/IVS.jpg" alt=""></li>
    <li><img src="/wp-content/uploads/2016/09/Innopraktika.png" alt=""></li>
    <li><img src="/wp-content/uploads/2016/09/enaza.png" alt=""></li>
    <li><img src="/wp-content/uploads/2016/09/vniimash.png" alt=""></li>
  </ul>
</div>

<?php get_footer(); ?>