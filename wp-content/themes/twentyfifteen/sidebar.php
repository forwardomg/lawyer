<?php
/**
 * The sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<div class="sidebar">
	<ul class="sidebar-lang"><?php pll_the_languages(array('show_flags'=>1,'show_names'=>0)); ?></ul>
	<div class="person-img">
		<?php if(get_field('face', 'option')): ?>
			<img src="<?php the_field('face', 'option') ?>" alt="">
		<?php endif; ?>
	</div>
	<div class="person-about">
		<?php dynamic_sidebar("person-about"); ?>
	</div>
	<?php if ( has_nav_menu( 'primary' ) ) : ?>
		<a href="#" class="nav-open">
			<svg fill="#FFFFFF" height="35" viewBox="0 0 24 24" width="35" xmlns="http://www.w3.org/2000/svg">
		    <path d="M0 0h24v24H0z" fill="none"/>
		    <path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"/>
			</svg>
		</a>
		<nav class="nav">
			<?php
				wp_nav_menu( array(
					'menu_class'     => 'nav-menu',
					'theme_location' => 'primary',
					'link_before' => '<span>',
					'link_after' => '</span>',
				) );
			?>
		</nav>
	<?php endif; ?>
	<?php dynamic_sidebar("sidebar-1"); ?>
</div>