var accordion = (function () { 
    var public = {},
        element = jQuery('.accordion-single');
 
    function handles() {
        element.on('click', function(event) {
        	event.preventDefault();
        	jQuery(this).toggleClass('active');
        });
    } 
 
    public.init = function () {
        handles();
    }; 
 
    return public;
}());
accordion.init();
