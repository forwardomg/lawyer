+function ($) {
  'use strict';

		var slider = $('.bxslider').bxSlider({ slideWidth: 150, minSlides: 2, maxSlides: 4, slideMargin: 40, pager: false, controls: false });
		

		$('.blog .entry-content').dotdotdot();

		$('.nav-open').on('click', function(event) {
			event.preventDefault();
			$('.nav').slideToggle('fast');
		});

		$(window).resize(function(event) {
			$('.blog .entry-content').trigger("update");
		});

		$('.slider-right').on('click', function(event) {
			event.preventDefault();
			slider.goToNextSlide();
		});

		$('.slider-left').on('click', function(event) {
			event.preventDefault();
			slider.goToPrevSlide();
		});

}(jQuery);