<?php
/*
Template Name: Template FAQs
*/

get_header(); ?>

<div class="content">

	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<?php if(get_field('faqs')): ?>
		<div class="accordions">
			<?php while(has_sub_field('faqs')): ?>
				<div class="accordion-single">
					<div class="accordion-title"><?php the_sub_field('question') ?></div>
					<div class="accordion-content"><?php the_sub_field('answer') ?></div>
				</div>
			<?php endwhile; ?>
		</div>
	<?php endif; ?>
	<a href="/contacts" class="btn btn-block btn-orange">Задайте свой вопрос</a>
</div>

<?php get_footer(); ?>