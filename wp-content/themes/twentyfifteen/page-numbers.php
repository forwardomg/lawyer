<?php
/*
Template Name: Template with numbers
*/

get_header(); ?>

<div class="numbers">
  <div class="columns">
    <div class="column is-4">
      <div class="numbers-single"><span>8</span><?php pll_e('лет в интеллектуальной собственности'); ?></div>
    </div>
    <div class="column is-4">
      <div class="numbers-single"><span>420</span><?php pll_e('зарегистрированных объектов интеллектуальной собственности'); ?></div>
    </div>
    <div class="column is-4">
      <div class="numbers-single"><span>90</span><?php pll_e('довольных клиентов'); ?></div>
    </div>
  </div>
</div>

<div class="content">

  <?php
    while ( have_posts() ) : the_post();
      get_template_part( 'content', 'page' );
    endwhile;
  ?>

</div>

<?php get_footer(); ?>